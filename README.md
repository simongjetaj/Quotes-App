# Quotes App

Besides Realtime Database _'Quotes App'_ is built with Cloud Firestore too. The app is deployed with Firebase at https://quotes-app-9e259.firebaseapp.com/ (using Cloud Firestore settings).
* I have set _cloud-firestore_ as a default branch, because here is where I am maintaining the app. 🙂

* You can also specify the branch (master or cloud-firestore) and run it from
 https://stackblitz.com/github/simongjetaj/quotes-app/tree/cloud-firestore (Cloud Firestore)  
 https://stackblitz.com/github/simongjetaj/quotes-app/tree/master (Realtime Database) 
 
[![quotes-app1.png](https://i.postimg.cc/GpyV47fF/quotes-app1.png)](https://i.postimg.cc/GpyV47fF/quotes-app1.png)
[![quotes-app2.png](https://i.postimg.cc/hjRwT8FH/quotes-app2.png)](https://i.postimg.cc/hjRwT8FH/quotes-app2.png)
